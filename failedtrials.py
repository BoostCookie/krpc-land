# This file contains other versions of functions that turned out to be slower.
# I put too much time into them to just throw them away (for now)

import math
import scipy
from sympy.functions.combinatorial.numbers import bell

# returns the value of φ for a given value u where E(φ | m) = u
# many thanks to https://analyticphysics.com/Special%20Functions/Inverse%20Elliptic%20Integrals%20of%20the%20First%20and%20Second%20Kinds.htm
# tol is the tolerance for termination
# is unfortunately very slow when φ (for E(φ, m) = u_shifted) gets close to τ/4
def inverse_of_elliptical_integral_of_the_second_kind_slow(u: float, m: float, tol=1e-6) -> float:
    def a(n: int) -> float:
        asum = 0
        for k in range(n + 1):
            asum -= math.comb(n, k) * math.gamma(k + 1/2) * math.gamma(n - k - 1/2) / math.tau * m**(n - k)
        return asum / (2 * n + 1)

    def b(n: int, l: int) -> float:
        return (-1)**l * math.prod(range(2 * n + 1, 2 * n + l + 1))

    # because of symmetries it suffices to look at the interval [0, border]
    quarter_border = scipy.special.ellipeinc(math.tau / 4, m)
    phi_shift = math.floor(u / (2 * quarter_border)) * math.pi
    u_shifted = u % (2 * quarter_border)
    is_in_first_quarter = 1.0
    if u_shifted >= quarter_border:
        is_in_first_quarter = -1.0
        phi_shift += math.pi
        u_shifted = 2 * quarter_border - u_shifted

    alist = []
    retsum = u_shifted
    n = 1
    while True:
        alist.append(a(n))
        lsum = 0
        for l in range(1, n+1):
            lsum += b(n, l) * float(bell(n, l, alist[:(n-l+1)]))
        new_term = u_shifted**(2 * n + 1) / ((2 * n + 1) * math.factorial(n)) * lsum
        retsum += new_term
        if abs(retsum) <= 1 and abs(scipy.special.ellipeinc(math.asin(retsum), m) - u_shifted) <= tol:
            break
        n += 1

    return phi_shift + is_in_first_quarter * math.asin(retsum)

# returns the value of φ for a given value u where E(φ | m) = u
# I made it unnecesarily complicated by using a smart™ taylor approximation for the initial value
# of scipy.optimizer.root and in the end it takes the same time or sometimes even slower.
# Also math domain errors can show up for the √.
def inverse_of_elliptical_integral_of_the_second_kind_still_slow(u: float, m: float, tol=None) -> float:
    # Because of symmetries it suffices to look at the interval [0, border] where border = E(τ / 4 | m)
    quarter_border = scipy.special.ellipeinc(math.tau / 4, m)
    phi_shift = math.floor(u / (2 * quarter_border)) * math.pi
    u_shifted = u % (2 * quarter_border)
    is_in_first_quarter = 1.0
    if u_shifted >= quarter_border:
        is_in_first_quarter = -1.0
        phi_shift += math.pi
        u_shifted = 2 * quarter_border - u_shifted

    # To make scipy.optimize.root quicker we start with a good estimate of φ.
    # For this we Taylor expand E(φ | m) to second order
    # E(φ | m) ≈ E(φ₀ | m) + E'(φ₀ | m) (φ - φ₀) + ½ E''(φ₀ | m) (φ - φ₀)²
    #   a := ½ E''(φ₀ | m)
    #   b := E'(φ₀ | m) - E''(φ₀ | m) φ₀
    #   c := E(φ₀ | m) - E'(φ₀ | m) φ₀ + ½ E''(φ₀ | m) φ₀²
    # u = E(φ | m) ≈ a φ² + b φ + c
    # => a φ² + b φ + c - u ≈ 0
    # With this we can use the quadratic formula to get a good starting estimate for φ
    # φ ≈ (-b ± √(b² - 4a(c-u))) / (2a)
    # We use the "+" solution, as it is the correct one.
    # Now which value should we use for φ₀?
    # The closer it is to our desired φ, the better the Taylor approximation.
    # I decided on φ₀ = u, because for values φ ≤ 0.5, E(φ | m) looks like f(φ) ≈ φ,
    # and for 0.5 < φ ≤ τ/4 this gives a slight underestimation that is worse when m is close to 1
    # which is only the case for high eccentricities ε since ε² = m.
    phi0 = u_shifted
    integrant = math.sqrt(1 - m * math.sin(phi0)**2)
    second_derivative = - m * math.sin(phi0) * math.cos(phi0) / integrant
    first_derivative = integrant
    a = second_derivative / 2
    b = first_derivative - second_derivative * phi0
    c = scipy.special.ellipeinc(phi0, m) - first_derivative * phi0 + 1/2 * second_derivative * phi0**2
    phi1 = u_shifted - c / b if a == 0 else (-b + math.sqrt(b**2 - 4 * a * (c - u_shifted))) / (2 * a)
    print(f"{2 * phi1 / math.pi=}")
    sol = scipy.optimize.root(lambda phi: scipy.special.ellipeinc(phi, m) - u_shifted, phi1, tol=tol)
    assert sol.success
    return phi_shift + is_in_first_quarter * sol.x[0]
