#set heading(numbering: "1.")
#set math.equation(numbering: "(1)")
#let dd = $upright(d)$

#align(center, text(17pt)[
  Rocket Guidance
  2024-01-15
])

#align(center)[Gehr Stefan]

= Coordinates
We work in a cylindrical coordinate system whose 0 is at the centre of the planet.
The $z$ direction is perpendicular to the orbit.
The current position is
$ arrow(r) = r hat(e)_r $
where $r$ is the distance from the origin and $hat(e)_r = (cos(phi), sin(phi))$.
The velocity and acceleration are
$ dot(arrow(r)) = dot(r) hat(e)_r + r dot(phi) hat(e)_phi \
  dot.double(arrow(r)) = (dot.double(r) - r dot(phi)^2) hat(e)_r + (2 dot(r)dot(phi) + r dot.double(phi)) hat(e)_phi. $
With Newton's second law we get
$ dot.double(arrow(r)) = - (G M)/r^2 hat(e)_r + arrow(F) / m(t) $
where the first term is gravity and the second term is the acceleration from the rocket's thrust vector $arrow(F)$ and the rocket's changing mass $m(t)$.
Just looking at the radial direction we get
$ dot.double(r) - r dot(phi)^2 = - (G M)/r^2 + (arrow(F) dot.c hat(e)_r) / m(t) $
with the radial component $arrow(F) dot.c hat(e)_r = F cos(pi/2 - psi) = F sin(psi)$
of the thrust vector.
As an ansatz for the current steering pitch angle $psi$ we use
$ sin(psi(t)) = A + B t - 1/(a(t)) (r dot(phi)^2 - (G M)/r^2) $
with $a(t) = F/m(t)$
which gives us the equation of motion
$ dot.double(r)(t) = (A + B t) a(t). $
We use $m(t) = m_0 - R t, v_e := F/R, tau := m_0 / R$ to calculate the integrals
$
b_0(t)
= integral_0^t a(s) dd s
= F integral_0^t 1/(m_0 - R t) dd s
= - F/R ln((m_0 - R t)/ m_0)
= - v_e ln(1 - t/tau) \
a(t) = dd / (dd t) b_0 (t) = v_e / tau 1 /(1 - t/tau) \
b_n (t)
= integral_0^t a(s) s^n dd s
= v_e tau^n integral_0^(t/tau) s^n/(1 - s) dd s.
$
We look at
$
I_n (r) &:= integral_0^(r) x^n/(1-x) dd x
= integral_(1-r)^1 (1-x)^n / x dd x
= sum_(k=0)^n vec(n, k) integral_(1-r)^1 (-x)^k / x dd x \
&= -log(1-r) + sum_(k=1)^n vec(n, k)(-1)^k integral_(1-r)^1 x^(k-1) dd x \
&= -log(1-r) + sum_(k=1)^n vec(n, k)(-1)^k/k (1 - (1-r)^k).
$
Now for $n in NN$ we derive
$
I_(n)(r) - I_(n-1) (r)
&= sum_(k=1)^(n-1) (vec(n, k) - vec(n-1, k)) (-1)^k/k (1 - (1-r)^k)
+ (-1)^(n)/(n) (1 - (1-r)^(n)) \
&= sum_(k=0)^(n-1) vec(n, k) (-1)^k /(n) (1 - (1-r)^k)
+ (-1)^(n)/(n) (1 - (1-r)^(n)) \
&= 1/(n) sum_(k=0)^(n) vec(n, k) (-1)^k (1 - (1-r)^k) \
&= -1/(n) sum_(k=0)^(n) vec(n, k) (r-1)^k \
&= -r^(n)/(n).
$
We use this relationship and get
$
b_(n)(t) &= v_e tau^(n) I_(n)(t/tau) \
&= v_e tau^(n) (I_(n-1) (t/tau) - (t/tau)^(n) / (n)) \
&= b_(n-1) (t) tau - (v_e t^n) / (n)
$
$
c_0(t)
&= integral_0^t (integral_0^s a(u) dd u) dd s
= integral_0^t b_0(s) dd s
= b_0 (t) t - integral_0^t a(s) s dd s \
&= b_0 (t) t - b_1(t)
$
$
c_n (t)
&= integral_0^t (integral_0^s a(u) u^n dd u) dd s
= integral_0^t b_n (s) dd s
= tau integral_0^t b_(n-1) (s) dd s
- v_e / n integral_0^t s^n dd s \
&= c_(n-1) (t) tau - (v_e t^(n+1)) / (n(n+1)).
$
This lets us integrate
$
dot(r)(t) = integral_0^t dot.double(r)(s) dd s
= A integral_0^t a(s) dd s + B integral_0^t a(s) s dd s
= A b_0(t) + B b_1(t) \
r(t) = integral_0^t dot(r)(s) dd s
= A integral_0^t b_0(s) dd s + B integral_0^t b_1(s) dd s
= A c_0(t) + B c_1(t)
$
