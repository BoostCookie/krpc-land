#!/usr/bin/env python3
import sys
from typing import Self, Tuple
from datetime import datetime
from decimal import Decimal, ROUND_HALF_DOWN
import time
import math
import numpy as np
from numpy.linalg import norm
import scipy
from scipy.spatial.transform import Rotation
import krpc
from krpc.services.spacecenter import Vessel, CelestialBody
from enum import Enum, auto

def to_tuple(vec: np.ndarray) -> Tuple[float, float, float]:
    return (vec[0], vec[1], vec[2])

def normalised(vec: np.ndarray) -> np.ndarray:
    return vec / norm(vec)

# returns the value of φ for a given value u where E(φ | m) = u
def inverse_of_elliptical_integral_of_the_second_kind(u: float, m: float, tol=None) -> float:
    # Because of symmetries it suffices to look at the interval u_shifted ∈ [0, quarter_border] where quarter_border = E(τ / 4 | m)
    quarter_border = scipy.special.ellipeinc(math.tau / 4, m)
    phi_shift = math.floor(u / (2 * quarter_border)) * math.pi
    u_shifted = u % (2 * quarter_border)
    is_in_first_quarter = 1.0
    if u_shifted >= quarter_border:
        is_in_first_quarter = -1.0
        phi_shift += math.pi
        u_shifted = 2 * quarter_border - u_shifted

    # start with φ₀ = τ/2 / E(τ/2 | m) * u, as it is a good linear estimate
    sol = scipy.optimize.root(lambda phi: scipy.special.ellipeinc(phi, m) - u_shifted, math.pi / scipy.special.ellipeinc(math.pi, m) * u_shifted, tol=tol)
    assert sol.success
    return phi_shift + is_in_first_quarter * sol.x[0]

# helper enum for KeplerOrbit
class TimeChange(Enum):
    CLOSEST = auto()
    FUTURE = auto()
    PAST = auto()

class KeplerOrbit:
    @staticmethod
    def static_phi_from_r(r: float, r0: float, ecc: float):
        return math.acos((r0 / r - 1) / ecc)

    # calculates the area from periapsis to φ
    @staticmethod
    def static_area(r0: float, ecc: float, phi: float) -> float:
        # continuous version of atan(s × tan(φ))
        def satan(phi: float, s: float):
            return math.atan(s * math.tan(phi)) + float(Decimal(phi / math.pi).to_integral_value(rounding=ROUND_HALF_DOWN)) * math.pi

        s = math.sqrt((1 - ecc) / (1 + ecc))
        omee = 1 - ecc**2
        return r0**2 / (2 * omee) * (
                2 * satan(phi / 2, s) / math.sqrt(omee)
                - ecc * math.sin(phi) / (ecc * math.cos(phi) + 1))

    # takes position x, velocity v, the body we are orbiting, and the time
    def __init__(self, x, v, body: CelestialBody, t=0.0):
        self.x = x
        self.v = v
        self.body = body
        self.t = t
        self.__transformation = None
        self.__transformation_inv = None

        # this is all based on this equation for the radius of an ellipse:
        # r(φ) = r₀ / (1 + ε cos(φ))
        # where r₀ is the radius the orbit with the same angular momentum would have it it were circular
        # and ε is the eccentricity of the ellipse

        # (angular momentum) / mass is a constant vector
        self.xv = np.cross(x, v)
        # get r₀ via the angular momentum
        self.r0 = self.xv @ self.xv / body.gravitational_parameter
        # ε = √(1 + E / E₀) with E = m/2 v² - GMm/|x| and E₀ = GMm / (2 r₀)
        self.ecc = math.sqrt(1 + self.r0 * (self.v @ self.v / body.gravitational_parameter - 2 / norm(x)))

        # the angle between periapsis and x
        # always between 0 and τ
        # (inverse of the ellipsis function)
        self.phi = KeplerOrbit.static_phi_from_r(float(norm(x)), self.r0, self.ecc)
        # acos gives a value between 0 and τ/2. 0 is periapsis, τ/2 is apoapsis.
        # if we are going from apoapsis to periapsis (towards the planet)
        # then we are actually between τ/2 and τ
        if x @ v < 0:
            self.phi = math.tau - self.phi

    # for debugging
    @classmethod
    def from_a_b(cls, a: float, b: float, body: CelestialBody) -> Self:
        ecc = math.sqrt(1 - (b/a)**2)
        r0 = a * (1 - ecc**2)
        er = np.array([1.0, 0.0, 0.0])
        ephi = np.array([0.0, 1.0, 0.0])
        x = r0 / (1 + ecc) * er
        v = math.sqrt(body.gravitational_parameter * r0 / (x @ x)) * ephi
        obj = cls.__new__(cls)
        cls.__init__(obj, x, v, body, t=0)
        return obj

    def __repr__(self):
        return "KeplerOrbit(np.array({}), np.array({}), `{}`, t={})".format(self.x, self.v, self.body.name, self.t)

    # transforms a vector from the Kepler basis
    # (all positions are in the xy-plane, (1, 0, 0) points towards the periapsis
    #  (-1, 0, 0) to the apoapsis, starting at periapsis a small angle increases the y value)
    # to the original body.non_rotating_reference_frame
    def transformation(self) -> np.ndarray:
        if self.__transformation is None:
            # new z axis is the unit vector along the angular momentum
            z_prime = normalised(self.xv)
            x_normed = normalised(self.x)
            # new x axis points towards the periapsis
            x_prime = Rotation.from_rotvec(-self.phi * z_prime).apply(x_normed)
            # new y axis points 90° ahead of the periapsis
            y_prime = Rotation.from_rotvec(math.tau / 4 * z_prime).apply(x_prime)
            self.__transformation = np.array([x_prime, y_prime, z_prime]).transpose()
        return self.__transformation

    def transformation_inv(self) -> np.ndarray:
        if self.__transformation_inv is None:
            self.__transformation_inv = np.linalg.inv(self.transformation())
        return self.__transformation_inv

    def r_from_phi(self, phi: float):
        return self.r0 / (1 + self.ecc * math.cos(phi))

    def phi_from_r(self, r: float):
        return KeplerOrbit.static_phi_from_r(r, self.r0, self.ecc)

    # area from periapsis to self.phi
    def area_at_phi(self):
        return KeplerOrbit.static_area(self.r0, self.ecc, self.phi)

    def area_at_other(self, other_phi: float):
        return KeplerOrbit.static_area(self.r0, self.ecc, other_phi)

    # the entire area
    def area(self):
        return math.pi * self.r0**2 / (1 - self.ecc)**(3/2)

    # returns a new KeplerOrbit object where the new angle is φ
    # the time difference respects all values for φ
    def go_to_phi(self, other_phi: float) -> Self:
        # use Kepler's second law to calculate the time difference
        area_difference = self.area_at_other(other_phi) - self.area_at_phi()
        xvnorm = norm(self.xv)
        time_difference = 2 * area_difference / xvnorm

        er = np.array([math.cos(other_phi), math.sin(other_phi), 0.0])
        ephi = np.array([-math.sin(other_phi), math.cos(other_phi), 0.0])
        new_r = self.r_from_phi(other_phi)
        new_x = new_r * self.transformation() @ er
        new_v = self.transformation() @ (xvnorm * (self.ecc * math.sin(other_phi) / self.r0 * er + 1 / new_r * ephi))

        return KeplerOrbit(new_x, new_v, self.body, float(self.t + time_difference))

    # returns a new KeplerOrbit object where the position has the magnitude of "radius"
    # time_change: FUTURE always goes forward in time, PAST always goes backwards in time,
    #              CLOSEST always goes takes the one with the smallest distance
    def go_to_radius(self, radius: float, time_change: TimeChange = TimeChange.FUTURE) -> Self:
        target_phi = self.phi_from_r(radius)
        target_phi2 = math.tau - target_phi

        # new_phi is between -τ and 2τ
        # new_phi < 0 can only happen for time_change == PAST and going from Pe to Ap
        # new_phi > τ can only happen for time_change == FUTURE and going from Ap to Pe
        new_phi = None
        if time_change == TimeChange.FUTURE:
            if self.phi <= target_phi:
                new_phi = target_phi
            elif self.phi <= target_phi2:
                new_phi = target_phi2
            else:
                new_phi = target_phi + math.tau
        elif time_change == TimeChange.PAST:
            if self.phi >= target_phi2:
                new_phi = target_phi2
            elif self.phi >= target_phi:
                new_phi = target_phi
            else:
                new_phi = -target_phi2
        elif time_change == TimeChange.CLOSEST:
            if self.phi >= math.pi:
                new_phi = target_phi2
            else:
                new_phi = target_phi
        else:
            raise Exception("TimeChange {} not yet implemented!".format(time_change))

        return self.go_to_phi(new_phi)

    # returns a new KeplerOrbit object where the position is where we will collide with the planet
    def go_to_collision(self, target_accuracy=5, max_iterations=20) -> Tuple[Self, float, float, float]:
        omega = np.array(self.body.angular_velocity(self.body.non_rotating_reference_frame))

        # gets the radius of the surface at the coordinates of new_orbit.x
        # to get the correct surface height we need to correct for the planet rotating
        # with vector omega for the time (new_orbit.t - self.t)
        def surface(new_orbit: KeplerOrbit) -> Tuple[float, float, float, float]:
            # compensate for the planet rotating in that time
            rot = Rotation.from_rotvec(-omega * (new_orbit.t - self.t))
            rotated_x = rot.apply(new_orbit.x)
            lat = new_orbit.body.latitude_at_position(to_tuple(rotated_x), new_orbit.body.non_rotating_reference_frame)
            long = new_orbit.body.longitude_at_position(to_tuple(rotated_x), new_orbit.body.non_rotating_reference_frame)
            surface_x = new_orbit.body.surface_position(lat, long, new_orbit.body.non_rotating_reference_frame)
            return float(norm(surface_x)), lat, long, float(norm(rotated_x - surface_x))

        surface_radius = self.body.equatorial_radius
        new_orbit = self
        lat, long = 0.0, 0.0
        accuracy = 0.0
        for _ in range(max_iterations):
            new_orbit = self.go_to_radius(surface_radius, time_change=TimeChange.FUTURE)
            surface_radius, lat, long, accuracy = surface(new_orbit)
            # 5 metre accuracy would be nice
            if accuracy < target_accuracy:
                break

        return new_orbit, lat, long, accuracy

    # velocity at the current position, relative to the surface at this position
    def surface_velocity(self) -> np.ndarray:
        omega = np.array(self.body.angular_velocity(self.body.non_rotating_reference_frame))
        return self.v - np.cross(omega, self.x)

    # get the semi-major axis and the semi-minor axis
    def ab(self) -> Tuple[float, float]:
        b_over_a_squared = 1 - self.ecc**2
        a = self.r0 / b_over_a_squared
        b = self.r0 / math.sqrt(b_over_a_squared)
        return a, b

    # get the eccentric anomaly at angle other_phi
    # this is the angle of the parameterisation (a cos(t_angle), b sin(t_angle)) where (0, 0) is at the centre of the ellipse
    def t_angle_at_other(self, other_phi: float) -> float:
        a, b = self.ab()
        return math.atan2(self.r_from_phi(other_phi) / b * math.sin(other_phi), self.ecc + self.r_from_phi(other_phi) / a * math.cos(other_phi))

    # get the current eccentric anomaly
    def t_angle_at_phi(self) -> float:
        return self.t_angle_at_other(self.phi)

    # calculates the arc length from the periapsis to other_phi
    def arc_length_at_other(self, other_phi: float) -> float:
        a, b = self.ab()
        t_angle = self.t_angle_at_other(other_phi)
        if t_angle < 0:
            t_angle += math.tau
        t_angle += math.floor(other_phi / math.tau)
        return b * scipy.special.ellipeinc(t_angle, 1 - (a / b)**2)

    def arc_length_at_phi(self) -> float:
        return self.arc_length_at_other(self.phi)

    # go to a certain value of the eccentric anomaly
    # this is the angle of the parameterisation (a cos(t_angle), b sin(t_angle)) where (0, 0) is at the centre of the ellipse
    def go_to_t_angle(self, t_angle: float) -> Self:
        a, b = self.ab()
        new_phi = math.atan2(b * math.sin(t_angle), a * (math.cos(t_angle) - self.ecc))
        if new_phi < 0:
            new_phi += math.tau
        new_phi += math.tau * math.floor(t_angle / math.tau)

        return self.go_to_phi(new_phi)

    # returns a new KeplerOrbit object where we have travelled some distance (positive or negative) along the ellipse
    def travel_distance(self, distance: float) -> Self:
        a, b = self.ab()
        target_arc_length = self.arc_length_at_phi() + distance
        # get the target eccentric anomaly
        # this is the angle of the parameterisation (a cos(t_angle), b sin(t_angle)) where (0, 0) is at the centre of the ellipse
        t_angle = inverse_of_elliptical_integral_of_the_second_kind(target_arc_length / b, 1 - (a/b)**2)
        return self.go_to_t_angle(t_angle)

    # let time delta_t (positive or negative) pass
    def time_warp(self, delta_t: float) -> Self:
        a, b = self.ab()
        new_t_angle = float(norm(self.xv)) / (a * b) * delta_t + self.t_angle_at_phi()
        return self.go_to_t_angle(new_t_angle)

    def speed_vertical_horizontal(self) -> Tuple[float, float]:
        up = normalised(self.x)
        vertical = self.v @ up
        return vertical, float(norm(self.v - vertical * up))

    def surface_speed_vertical_horizontal(self) -> tuple[float, float]:
        up = normalised(self.x)
        v = self.surface_velocity()
        vertical = v @ up
        return float(vertical), float(norm(v - vertical * up))

    def pitch(self) -> float:
        vertical, horizontal = self.speed_vertical_horizontal()
        return math.atan2(vertical, horizontal)

    # the pitch of the surface speed
    def pitch_surface(self) -> float:
        vertical, horizontal = self.surface_speed_vertical_horizontal()
        return math.atan2(vertical, horizontal)

def rocket_time_for_delta_v(delta_v: float, mass: float, fuel_consumption: float, thrust: float) -> float:
    return mass / fuel_consumption * (1 - math.exp(-fuel_consumption / thrust * delta_v))

def rocket_distance_for_time(time: float, mass: float, fuel_consumption: float, thrust: float, starting_velocity: float) -> float:
    rtm = fuel_consumption * time / mass
    omrtm = 1 - rtm
    return starting_velocity * time - thrust * mass / fuel_consumption**2 * (-omrtm * math.log(omrtm) - rtm)

def fuel_consumption(vessel: Vessel) -> float:
    return vessel.available_thrust / (vessel.specific_impulse * scipy.constants.g)

def average_gravity(gravitational_parameter: float, radius1: float, radius2: float) -> float:
    return - abs(gravitational_parameter / 3 * (radius1 - radius2) * (1 / radius1**3 - 1 / radius2**3))

def main():
    conn = krpc.connect(name="hoverslam")
    assert conn.space_center is not None
    drawing = conn.drawing
    assert drawing is not None
    vessel = conn.space_center.active_vessel

    body = vessel.orbit.body
    reference_frame = body.non_rotating_reference_frame
    x = np.array(vessel.position(reference_frame))
    v = np.array(vessel.velocity(reference_frame))
    start_time = vessel.met
    orbit = KeplerOrbit(x, v, body, t=start_time)
    print(f"{orbit.ecc=}")
    collision, lat, long, accuracy = orbit.go_to_collision()
    print(f"{accuracy=} m")
    print(f"{norm(collision.x - orbit.x)=}")
    collision_speed = float(norm(collision.surface_velocity()))
    print(f"{collision_speed=} m/s")
    collision_speed_vertical, collision_speed_horizontal = collision.surface_speed_vertical_horizontal()
    print(f"{collision_speed_horizontal=} m/s")
    print(f"{collision_speed_vertical=} m/s")
    fuel_cons = fuel_consumption(vessel)
    deceleration_time = rocket_time_for_delta_v(collision_speed, vessel.mass, fuel_cons, vessel.available_thrust)
    print(f"{deceleration_time=} s")
    deceleration_distance = rocket_distance_for_time(deceleration_time, vessel.mass, fuel_cons, -vessel.available_thrust, collision_speed)
    print(f"{deceleration_distance=} m")
    burn_start = collision.travel_distance(-deceleration_distance)
    average_pitch = (min(0, collision.pitch()) + min(0, burn_start.pitch())) / 2
    middle_pitch = burn_start.time_warp((collision.t - burn_start.t) / 2).pitch()
    print(f"{average_pitch * 360 / math.tau=}")
    print(f"{middle_pitch * 360 / math.tau=}")
    average_horizontal_thrust = math.cos(average_pitch) * vessel.available_thrust
    average_vertical_thrust = - math.sin(average_pitch) * vessel.available_thrust
    print(f"{average_vertical_thrust=}")
    print(f"{average_horizontal_thrust=}")
    print(f"{collision_speed_vertical=}")
    horizontal_deceleration_time = rocket_time_for_delta_v(abs(collision_speed_horizontal), vessel.mass, fuel_cons, abs(average_horizontal_thrust))
    vertical_deceleration_time = rocket_time_for_delta_v(abs(collision_speed_vertical), vessel.mass, fuel_cons, abs(average_vertical_thrust))
    print(f"{horizontal_deceleration_time=} s")
    print(f"{vertical_deceleration_time=} s")
    vertical_deceleration_distance = rocket_distance_for_time(vertical_deceleration_time, vessel.mass, fuel_cons, -abs(average_vertical_thrust), abs(collision_speed_vertical))
    horizontal_deceleration_distance = rocket_distance_for_time(horizontal_deceleration_time, vessel.mass, fuel_cons, -abs(average_horizontal_thrust), abs(collision_speed_horizontal))
    print(f"{horizontal_deceleration_distance=} m")
    print(f"{vertical_deceleration_distance=} m")

    last_update = datetime.now()
    lines = []
    while True:
        if (datetime.now() - last_update).total_seconds() > 0.5:
            last_update = datetime.now()
            print("impact in", collision.t - vessel.met, "s", "\tv=", norm(collision.surface_velocity()) , "m/s", end="\t\t")
            print("burn in", burn_start.t - vessel.met, "s")
            for line in lines:
                line.remove()
            lines = []
            lines.append(drawing.add_direction_from_com(to_tuple((collision.x - np.array(vessel.position(reference_frame)))), reference_frame))
            #collision_coordinate_pos = np.array(body.surface_position(lat, long, reference_frame))
            #lines.append(drawing.add_direction_from_com(to_tuple(collision_coordinate_pos - np.array(vessel.position(reference_frame))), reference_frame))
            #lines.append(drawing.add_text("BOOM", reference_frame, to_tuple(collision_coordinate_pos), (0.0,0.0,0.0,0.0)))
            #lines.append(drawing.add_direction_from_com(to_tuple(burn_start.x - np.array(vessel.position(reference_frame))), reference_frame))
            #lines.append(drawing.add_direction_from_com(to_tuple(burn_start2.x - np.array(vessel.position(reference_frame))), reference_frame))
        if burn_start.t - vessel.met < 0:
            vessel.control.throttle = 1

class Euler_Richardson:
    def __init__(self, accel_fun, t0: float, x0: np.ndarray, v0: np.ndarray, t_bound: float, min_step=0.0, max_step=math.inf, position_tolerance=1.0):
        self.accel_fun = accel_fun
        self.t = t0
        self.x = x0
        self.v = v0
        self.t_bound = t_bound
        self.min_step = min_step
        self.max_step = max_step
        self.step_size = None
        self.position_tolerance = position_tolerance
        self.status = "running"

    def step(self):
        a_start = self.accel_fun(self.t, self.x, self.v)

        # determine the maximum step_size
        vnorm = norm(self.v)
        self.step_size = max(self.min_step, min(self.max_step, self.position_tolerance / vnorm))
        assert self.step_size > 0

        # Euler-Richardson
        vmid = self.v + 1/2 * a_start * self.step_size
        xmid = self.x + 1/2 * self.v * self.step_size
        amid = self.accel_fun(self.t + 1/2 * self.step_size, xmid, vmid)
        self.t += self.step_size
        self.x += vmid * self.step_size
        self.v += amid * self.step_size

        if self.t >= self.t_bound:
            self.status = "finished"

warntime = None
def warn(*args, end="\n"):
    global warntime
    if warntime == None or (datetime.now() - warntime).total_seconds() > 5:
        print(*args, file=sys.stderr, end=end)
        if end == "\n":
            warntime = datetime.now()

def rkfun_helper(t, y, accel_fun):
    global prevtime
    ydot = np.zeros_like(y)
    accel = accel_fun(t, y[:3], y[3:])
    ydot[:3] = y[3:]
    ydot[3:] = accel
    return ydot

def vessel_mass(t, vessel):
    fuel_consumption = vessel.available_thrust / (vessel.specific_impulse * scipy.constants.g)
    return vessel.mass - fuel_consumption * t

def vessel_stage_dry_mass(vessel: Vessel):
    current_engine = next(filter(lambda e : e.active, vessel.parts.engines))
    stage_resources = vessel.resources_in_decouple_stage(stage = current_engine.part.decouple_stage)
    mfuel = 5 * stage_resources.amount("LiquidFuel")
    moxidizer = 5 * stage_resources.amount("Oxidizer")
    return vessel.mass - mfuel - moxidizer

out_of_prop = False
def engine_accel_fun(t, r, v, vessel: Vessel, dry_mass):
    mass = vessel_mass(t, vessel)
    #vessel.resources_in_decouple_stage
    if mass < dry_mass:
        # ran out of resources => no thrust anymore
        global out_of_prop
        out_of_prop = True
        warn(f"Running out of propellant: {vessel.mass=} kg, {dry_mass=} kg, {mass=} kg, {t=} s")
        return np.zeros(3)

    body = vessel.orbit.body
    rnorm = norm(r)
    pressure = body.pressure_at(float(rnorm) - body.equatorial_radius)
    thrust = vessel.available_thrust_at(pressure)

    pointing = - (v - np.cross(body.angular_velocity(body.non_rotating_reference_frame), r))
    upwards = r / rnorm
    pointing_direction = upwards @ pointing
    if pointing_direction < 0:
        # never point downwards
        pointing -= pointing_direction * upwards

    return thrust / mass * pointing / norm(pointing)

# calculate the altitude where the hoverslam reaches zero velocity
# returns tuple: (altitude above sea level, altitude above terrain at landing point)
def calc_hoverslam_altitude(vessel: Vessel, dry_mass):
    body = vessel.orbit.body
    reference_frame = body.non_rotating_reference_frame
    r = np.asarray(vessel.position(reference_frame))
    v = np.asarray(vessel.velocity(reference_frame))

    global out_of_prop
    out_of_prop = False
    accel_fun = lambda t, r, v : - body.gravitational_parameter * r / (r @ r)**1.5 + engine_accel_fun(t, r, v, vessel, dry_mass)
    #rkfun = lambda t, y : rkfun_helper(t, y, accel_fun)
    #y0 = np.zeros(6)
    #y0[:3] = r
    #y0[3:] = v
    #rk = scipy.integrate.RK23(rkfun, 0, y0, t_bound=100000, first_step=1, rtol=0.01)
    simulator = Euler_Richardson(accel_fun, 0, r, v, t_bound=100000, position_tolerance=100, min_step=0.1)
    altitude_above_sea_level = 0
    altitude_above_surface = 0
    hitspeed = 0
    falling = False
    while simulator.status != "finished":
        r = simulator.x
        v = simulator.v
        if r @ v < 0:
            falling = True
        elif falling:
            # going back up again after falling => done with integration
            break
        if out_of_prop:
            # ran out of propellant
            print("out of prop\t", end="")
            break
        lat = body.latitude_at_position(to_tuple(r), reference_frame)
        long = body.longitude_at_position(to_tuple(r), reference_frame)
        altitude_above_sea_level = float(norm(r)) - body.equatorial_radius
        altitude_above_surface = altitude_above_sea_level - body.surface_height(lat, long)
        if altitude_above_surface < 0 and hitspeed == 0:
            # hitting the surface
            hitspeed = norm(v)
        simulator.step()
        warn(f"{simulator.step_size=}")
    return altitude_above_surface, hitspeed

if __name__ == "__main__":
    main()
